import React from 'react';

const SelectBox = (props) => {

    const { ChooseCateg } = props;

    return(
        <select className='form-control center-block' style={{width: 200}} onChange={ChooseCateg}>
            <option value="" selected disabled>Select Category</option>
            <option value="all">All</option>
            <option value="design">Design</option>
            <option value="development">Development</option>
            <option value="database">Database</option>
        </select>
    )
};

export default SelectBox;