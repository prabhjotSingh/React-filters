import React from 'react';

const Tiles = (props) => {

    const {
        title,
        description,
    } = props;

    return(
        <li className="col-xs-12 col-sm-3">
            <div className='panel panel-primary'>
                <div className="panel-heading">
                    <h3 className="panel-title">{title}</h3>
                </div>
                <div className="panel-body">
                    {description}
                </div>
            </div>
        </li>
    )
};

export default Tiles;