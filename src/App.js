import React, { Component } from 'react';
import './App.css';

import Tiles from './components/tiles'
import SelectBox from './components/selectBox'

class App extends Component {

    constructor() {
        super();
        this.state = {
            categorySelected: 'all',
        };
        this.ChooseCatag = this.ChooseCatag.bind(this);
    }

    ChooseCatag = (event) => {
        this.setState({
           categorySelected: event.target.value
        });
    };

  render() {

      const Data = [
          {
              category: 'design',
              title: 'title 1',
              desc: 'Designer Content'
          },
          {
              category: 'development',
              title: 'title 2',
              desc: 'Development Content'
          },
          {
              category: 'database',
              title: 'title 3',
              desc: 'Database Content'
          },
          {
              category: 'development',
              title: 'title 4',
              desc: 'Development Content'
          },
          {
              category: 'design',
              title: 'title 5',
              desc: 'Designer Content'
          },
      ];

      const filterCondition = event => {return event.category == this.state.categorySelected};
      let filteredArray = this.state.categorySelected !== 'all' ? Data.filter(filterCondition) : Data;
      const tilesRendered = filteredArray.map((data) => {
         return(
             <Tiles title={data.title} description={data.desc}/>
         )
      });
    return (
      <div className="App">
        <header className="App-header">
          <h1>Filters</h1>
        </header>
          <br/>
          <div className='container'>
              <SelectBox ChooseCateg={this.ChooseCatag}/>
              <br/>
              <div className='row'>
                  {tilesRendered}
              </div>
          </div>
      </div>
    );
  }
}

export default App;
